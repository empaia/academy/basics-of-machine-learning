# EMPAIA Academy: Machine Learning Basics

Author: Klaus Strohmenger, Charité - Universitätsmedizin Berlin

## Setup

* Tested with Python 3.9

### Instruction on Linux

Run the following commands in `bash`:

```bash
# install python3 and the venv package
sudo apt install python3-venv
# create virtual environment
python3 -m venv .venv
# activate virtual environment
source .venv/bin/activate
# install third-party Python libraries
pip3 install -r requirements.txt
# run jupyter-notebook, browser should open automatically
jupyter notebook
```
